const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const concat = require('gulp-concat');
const browserSync = require('browser-sync');
const autoprefixer= require ('gulp-autoprefixer');
const del = require('del');
gulp.task('html', function(){
    gulp.src ('src/**/*.html')
    .pipe(gulp.dest('./dist/'));
});
gulp.task('sass', function(){
    return gulp.src('src/**/*.scss')
    .pipe(sass().on ('error', sass.logError))
    .pipe (autoprefixer({
        browsers: ['last 15 versions']
    }))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./dist/styles'));
});
gulp.task('reloader', function(){
    browserSync({
        server:{
            baseDir:'./dist'
        }
});
});
gulp.task('image', function(){
    gulp.src('./src/assets/img/*')
    .pipe(image ())
    .pipe(gulp.dest('./dist/assets/img'));
});
gulp.task('clean', function() {
    return del.sync('dist'); // Delete folder before building
    });
gulp.task('watch',['clean', 'sass','html','reloader', 'image'], function(){
    gulp.watch('./src/**/*.html',['html']);
    gulp.watch('./dist/**/*.html', browserSync.reload);
    gulp.watch('./src/**/*.scss', ['sass']);
    gulp.watch('./dist/styles/**/*.css')
    .on('change', browserSync.reload);
    gulp.watch('./dist/img',browserSync.reload)
});
